document.addEventListener('DOMContentLoaded', async function (event) {
    const api = axios.create({
        baseURL: 'ui/',
        timeout: 120000
    });
    Vue.filter('formatDate', function (value) {
        if (value) {
            return moment(String(value)).format('MM/DD/YYYY')
        }
    }
    );
    Vue.filter('formatMessageTypes', function (value) {
        if (value) {
            return value.replace(/;/g, ', ');
        }
    })
    const orders = new Vue({
        el: '#orders',
        data: {
            orders: null
        },
        created: async function () {
            this.fetchOrders();
            this.timer = setInterval(this.fetchOrders, 5 * 1000);
        },
        methods: {
            fetchOrders: async function () {
                ordersResponse = await api.get('orders');
                this.orders = ordersResponse.data;
                document.getElementById('pageloader').classList.add("hidden");
            }
        },
        beforeDestroy: function () {
            clearInterval(this.timer);
        }
    });
    const form = new Vue({
        el: '#newOrder',
        data: {
            modalActive: false,
            place: {
                deliveryDate: moment(new Date()).format('YYYY-MM-DD'),
                quantity: 1000,
                busy: false,
                product: 'aluminium'
            },
            search: {
                scsnId: null,
                name: null,
                kvk: null,
                tin: null,
                busy: false,
                sellers: []
            }
        },
        created: async function () {
        },
        methods: {
            sendForm: async function (e) {
                e.preventDefault();
                this.place.busy = true;
                try {
                    const orderResponse = await api.post('orders',
                        {
                            issueDate: new Date(),
                            requestedDeliveryDate: new Date(this.place.deliveryDate),
                            seller: {
                                name: this.place.seller["cac:PartyName"]?.["cbc:Name"] || '',
                                idsid: this.place.connector.idsId,
                                accessurl: this.place.connector.accessUrl,
                                scsnid: this.place.seller["cac:PartyIdentification"]?.["cbc:ID"] || '',
                                tin: this.place.seller["cac:PartyName"]?.["cbc:Name"] || '',
                                kvk: this.place.seller["cac:PartyLegalEntity"]?.["cbc:CompanyID"] || ''
                            },
                            product: this.place.product,
                            quantity: this.place.quantity,
                            version: this.place.connector.version,
                        });

                    if (orderResponse.data.error !== undefined) {
                        alert('Error in placing order');
                        console.log(orderResponse.data.error);
                        this.place.busy = false;
                        this.modalActive = false;
                        return;
                    }
                } catch (e) {
                    alert('Error in placing order');
                    console.log(orderResponse)
                }
                this.place.busy = false;
                this.modalActive = false;
            },
            searchForm: async function (e) {
                e.preventDefault();
                this.search.busy = true;
                const searchResponse = await api.post('search', {
                    scsnId: this.search.scsnId,
                    name: this.search.name,
                    kvk: this.search.kvk,
                    tin: this.search.tin
                });
                this.search.busy = false;
                if (searchResponse.data.error !== undefined) {
                    error('Error in search for parties');
                    return;
                }
                if (Array.isArray(searchResponse.data)) {
                    this.search.sellers = searchResponse.data;
                } else {
                    this.search.sellers = [searchResponse.data];
                }

            }
        }
    });
});