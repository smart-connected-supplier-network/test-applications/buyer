'use strict';
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const bodyParser = __importStar(require("body-parser"));
const axios_1 = __importDefault(require("axios"));
const templates_1 = require("./templates");
const express = require('express');
const util = require('util');
const app = express();
app.use(bodyParser.text({ type: "application/xml" }));
app.use(bodyParser.json());
app.use(express.static('static'));
const forwardAddress = process.env.FORWARD || "http://192.168.136.129:8081";
const party = {
    name: process.env.NAME || "SCSN Buyer Part",
    idsid: process.env.IDSID || "https://ids.tno.nl/connector/Buyer-1",
    accessurl: process.env.ACCESSURL || "http://192.168.136.129:8080",
    scsnid: process.env.SCSNID || "urn:scsn:UNKNOWN",
    tin: process.env.TIN || "NL12345",
    kvk: process.env.KVK || "12345"
};
const accounting = {
    name: "Accounting Noord-Brabant",
    scsnid: "urn:scsn:5790001398644",
    tin: "NL234324324S02",
    kvk: "90845456"
};
let server = app.listen(8080, function () {
    console.log('SCSN Buyer started on 0.0.0.0:8080 (http) ');
});
server.setTimeout(120000);
let orders = new Map();
app.use((req, res, next) => setTimeout(next, 100));
app.post('/api/:version/orderresponse', (req, res) => {
    const parsedOrderResponse = templates_1.parseOrder(req.body);
    console.log(`Received order response ${parsedOrderResponse.id}`);
    let order = orders.get(parsedOrderResponse.id);
    order.status = "Accepted";
    order.promisedDeliveryDate = parsedOrderResponse.promisedDeliveryDate;
    orders.set(parsedOrderResponse.id, order);
    res.send('');
});
app.post('/api/:version/orderstatus', (req, res) => {
    const parsedOrderStatus = templates_1.parseOrderStatus(req.body);
    console.log(`Received order status ${parsedOrderStatus.id} ${parsedOrderStatus.statusCode} ${parsedOrderStatus.value}`);
    let order = orders.get(parsedOrderStatus.id);
    order.statusList.push(parsedOrderStatus);
    orders.set(parsedOrderStatus.id, order);
    res.send('');
});
app.post('/api/:version/despatchadvice', (req, res) => {
    const orderId = templates_1.parseDespatchAdvice(req.body);
    const order = orders.get(orderId);
    console.log(`Received despatch advice ${orderId}`);
    order.status = 'Received despatch advice';
    orders.set(orderId, order);
    res.send('');
});
app.post('/api/:version/invoice', (req, res) => {
    const orderId = req.body;
    const order = orders.get(orderId);
    console.log(`Received invoice ${orderId}`);
    order.status = 'Received invoice';
    orders.set(orderId, order);
    res.send('');
});
app.get('/api/file/:filename', (req, res) => {
    const filename = req.params.filename;
    if (filename == 'aluminium') {
        res.set({ "Content-Disposition": `attachment; filename="aluminium"` });
        res.send("Test file aluminium");
    }
    else if (filename == 'steel') {
        res.set({ "Content-Disposition": `attachment; filename="steel"` });
        res.send("Test file steel");
    }
});
app.get('/ui/orders', (req, res) => {
    res.send(Array.from(orders.values()));
});
app.post('/ui/orders', (req, res) => {
    const orderId = `${party.scsnid}:Order-${orders.size + 1}`;
    let order = req.body;
    order.id = orderId;
    order.issueDate = new Date(order.issueDate);
    order.requestedDeliveryDate = new Date(order.requestedDeliveryDate);
    order.buyer = party;
    order.accounting = accounting;
    orders.set(orderId, order);
    let orderRequestMessage = templates_1.orderMessage(order);
    let headers = {};
    if (order.seller.scsnid !== undefined) {
        headers = {
            'Forward-Id': order.seller.scsnid,
            'Forward-Sender': party.scsnid,
            'Content-Type': 'application/xml'
        };
    }
    else {
        headers = {
            'Forward-Recipient': order.seller.recipient,
            'Forward-To': order.seller.idsid,
            'Forward-Accessurl': order.seller.accessurl,
            'Forward-Sender': party.scsnid,
            'Content-Type': 'application/xml'
        };
    }
    axios_1.default.post(`${forwardAddress}/${order.version}/order`, orderRequestMessage, {
        headers: headers,
        timeout: 120000
    }).then(response => {
        order.status = "Placed";
        orders.set(orderId, order);
        res.send({
            status: "OK"
        });
    }).catch(error => {
        console.log(util.inspect(error, { depth: 20 }));
        order.status = "Error";
        orders.set(orderId, order);
        res.status(500).send({
            status: "Error",
            message: error
        });
    });
});
app.post('/ui/search', (req, res) => {
    let search = req.body;
    if (search.scsnId !== undefined && search.scsnId !== null && search.scsnId.trim() !== '') {
        axios_1.default.get(`${forwardAddress}/broker/party/${search.scsnId}`, {
            headers: {
                'Forward-To': process.env.SCSN_BROKERID,
                'Forward-Sender': party.scsnid,
                'Forward-Recipient': 'broker',
                'Forward-Accessurl': process.env.SCSN_BROKERURL,
                'Accept': 'application/json'
            },
            timeout: 120000
        }).then(response => {
            res.status(response.status).send(response.data);
        }).catch(error => {
            console.log(util.inspect(error, { depth: 20 }));
            res.status(500).send({
                status: "Error",
                message: error
            });
        });
    }
    else {
        axios_1.default.get(`${forwardAddress}/broker/party?name=${encodeURI(search.name || '')}&KVK=${encodeURI(search.kvk || '')}&TIN=${encodeURI(search.tin || '')}`, {
            headers: {
                'Forward-To': process.env.SCSN_BROKERID,
                'Forward-Sender': party.scsnid,
                'Forward-Recipient': 'broker',
                'Forward-Accessurl': process.env.SCSN_BROKERURL,
                'Accept': 'application/json'
            },
            timeout: 120000
        }).then(response => {
            res.status(response.status).send(response.data);
        }).catch(error => {
            console.log(util.inspect(error, { depth: 20 }));
            res.status(500).send({
                status: "Error",
                message: error
            });
        });
    }
});
process.on('SIGTERM', () => {
    server.close(() => {
        console.log('Process terminated');
    });
});
//# sourceMappingURL=index.js.map