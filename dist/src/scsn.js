'use strict';
Object.defineProperty(exports, "__esModule", { value: true });
exports.OrderNamespace = {
    '@xmlns': 'urn:oasis:names:specification:ubl:schema:xsd:Order-2',
    '@xmlns:cac': 'urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2',
    '@xmlns:cbc': 'urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2',
    '@xmlns:xsi': 'http://www.w3.org/2001/XMLSchema-instance',
    '@xsi:schemaLocation': 'urn:oasis:names:specification:ubl:schema:xsd:Order-2 http://docs.oasis-open.org/ubl/os-UBL-2.1/xsd/maindoc/UBL-Order-2.1.xsd'
};
//# sourceMappingURL=scsn.js.map