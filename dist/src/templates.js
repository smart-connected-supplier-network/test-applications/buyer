'use strict';
Object.defineProperty(exports, "__esModule", { value: true });
const convert = require('xml-js');
const moment = require('moment');
class Despatch {
}
exports.Despatch = Despatch;
class Order {
}
exports.Order = Order;
class OrderStatus {
}
exports.OrderStatus = OrderStatus;
class Party {
}
exports.Party = Party;
exports.parseOrderStatus = function (xml) {
    var _a, _b, _c, _d, _e, _f, _g, _h;
    let parsedXml = convert.xml2js(xml, { compact: true });
    return {
        id: (_b = (_a = parsedXml['OrderStatus']) === null || _a === void 0 ? void 0 : _a['cac:OrderReference']) === null || _b === void 0 ? void 0 : _b['cbc:ID'],
        statusCode: (_e = (_d = (_c = parsedXml['OrderStatus']) === null || _c === void 0 ? void 0 : _c['scsn:OrderLineStatus']) === null || _d === void 0 ? void 0 : _d['scsn:DeliveryStatus']) === null || _e === void 0 ? void 0 : _e['cbc:StatusCode'],
        value: (_h = (_g = (_f = parsedXml['OrderStatus']) === null || _f === void 0 ? void 0 : _f['scsn:OrderLineStatus']) === null || _g === void 0 ? void 0 : _g['scsn:DeliveryStatus']) === null || _h === void 0 ? void 0 : _h['scsn:StatusValue']
    };
};
exports.parseOrder = function (xml) {
    var _a, _b, _c, _d, _e, _f, _g, _h, _j, _k, _l, _m, _o, _p, _q, _r, _s, _t, _u, _v, _w, _x, _y, _z, _0, _1, _2, _3, _4, _5, _6, _7, _8, _9, _10, _11, _12, _13, _14, _15, _16, _17, _18, _19, _20, _21, _22, _23, _24, _25, _26, _27, _28, _29, _30, _31, _32, _33, _34, _35, _36, _37, _38, _39, _40, _41, _42, _43, _44, _45, _46, _47, _48, _49, _50, _51, _52, _53, _54, _55, _56, _57, _58, _59, _60, _61, _62, _63, _64;
    let parsedXml = convert.xml2js(xml, { compact: true });
    let type = (parsedXml['Order']) ? 'Order' : 'OrderResponse';
    let promisedDate = (type === 'OrderResponse') ? new Date((_e = (_d = (_c = (_b = (_a = parsedXml[type]['cac:OrderLine']) === null || _a === void 0 ? void 0 : _a['cac:LineItem']) === null || _b === void 0 ? void 0 : _b['cac:Delivery']) === null || _c === void 0 ? void 0 : _c['cac:PromisedDeliveryPeriod']) === null || _d === void 0 ? void 0 : _d['cbc:EndDate']) === null || _e === void 0 ? void 0 : _e['_text']) : undefined;
    let buyerAccessUrl = (_k = (_j = (_h = (_g = (_f = parsedXml[type]['cac:BuyerCustomerParty']) === null || _f === void 0 ? void 0 : _f['cac:Party']) === null || _g === void 0 ? void 0 : _g['cac:Contact']) === null || _h === void 0 ? void 0 : _h['cac:OtherCommunication']) === null || _j === void 0 ? void 0 : _j['cbc:Value']) === null || _k === void 0 ? void 0 : _k['_text'];
    if (((_l = buyerAccessUrl) === null || _l === void 0 ? void 0 : _l.trim()) === '') {
        buyerAccessUrl = undefined;
    }
    let sellerAccessUrl = (_r = (_q = (_p = (_o = (_m = parsedXml[type]['cac:SellerSupplierParty']) === null || _m === void 0 ? void 0 : _m['cac:Party']) === null || _o === void 0 ? void 0 : _o['cac:Contact']) === null || _p === void 0 ? void 0 : _p['cac:OtherCommunication']) === null || _q === void 0 ? void 0 : _q['cbc:Value']) === null || _r === void 0 ? void 0 : _r['_text'];
    if (((_s = sellerAccessUrl) === null || _s === void 0 ? void 0 : _s.trim()) === '') {
        sellerAccessUrl = undefined;
    }
    let buyerConnectorId = (_w = (_v = (_u = (_t = parsedXml[type]['cac:BuyerCustomerParty']) === null || _t === void 0 ? void 0 : _t['cac:Party']) === null || _u === void 0 ? void 0 : _u['cac:PhysicalLocation']) === null || _v === void 0 ? void 0 : _v['cbc:Description']) === null || _w === void 0 ? void 0 : _w['_text'];
    if (((_x = buyerConnectorId) === null || _x === void 0 ? void 0 : _x.trim()) === '') {
        buyerConnectorId = undefined;
    }
    let sellerConnectorId = (_1 = (_0 = (_z = (_y = parsedXml[type]['cac:SellerSupplierParty']) === null || _y === void 0 ? void 0 : _y['cac:Party']) === null || _z === void 0 ? void 0 : _z['cac:PhysicalLocation']) === null || _0 === void 0 ? void 0 : _0['cbc:Description']) === null || _1 === void 0 ? void 0 : _1['_text'];
    if (((_2 = sellerConnectorId) === null || _2 === void 0 ? void 0 : _2.trim()) === '') {
        sellerConnectorId = undefined;
    }
    console.log(`Buyer: ${buyerAccessUrl} ${buyerConnectorId}, seller: ${sellerAccessUrl} ${sellerConnectorId}`);
    return {
        id: (_3 = parsedXml[type]['cbc:ID']) === null || _3 === void 0 ? void 0 : _3['_text'],
        status: (type === 'OrderResponse') ? "Accepted" : "Placed",
        issueDate: new Date((_4 = parsedXml[type]['cbc:IssueDate']) === null || _4 === void 0 ? void 0 : _4['_text']),
        requestedDeliveryDate: new Date((_9 = (_8 = (_7 = (_6 = (_5 = parsedXml[type]['cac:OrderLine']) === null || _5 === void 0 ? void 0 : _5['cac:LineItem']) === null || _6 === void 0 ? void 0 : _6['cac:Delivery']) === null || _7 === void 0 ? void 0 : _7['cac:RequestedDeliveryPeriod']) === null || _8 === void 0 ? void 0 : _8['cbc:EndDate']) === null || _9 === void 0 ? void 0 : _9['_text']),
        promisedDeliveryDate: promisedDate,
        buyer: {
            name: (_13 = (_12 = (_11 = (_10 = parsedXml[type]['cac:BuyerCustomerParty']) === null || _10 === void 0 ? void 0 : _10['cac:Party']) === null || _11 === void 0 ? void 0 : _11['cac:PartyName']) === null || _12 === void 0 ? void 0 : _12['cbc:Name']) === null || _13 === void 0 ? void 0 : _13['_text'],
            scsnid: (_17 = (_16 = (_15 = (_14 = parsedXml[type]['cac:BuyerCustomerParty']) === null || _14 === void 0 ? void 0 : _14['cac:Party']) === null || _15 === void 0 ? void 0 : _15['cac:PartyIdentification']) === null || _16 === void 0 ? void 0 : _16['cbc:ID']) === null || _17 === void 0 ? void 0 : _17['_text'],
            tin: (_21 = (_20 = (_19 = (_18 = parsedXml[type]['cac:BuyerCustomerParty']) === null || _18 === void 0 ? void 0 : _18['cac:Party']) === null || _19 === void 0 ? void 0 : _19['cac:PartyTaxScheme']) === null || _20 === void 0 ? void 0 : _20['cbc:CompanyID']) === null || _21 === void 0 ? void 0 : _21['_text'],
            kvk: (_25 = (_24 = (_23 = (_22 = parsedXml[type]['cac:BuyerCustomerParty']) === null || _22 === void 0 ? void 0 : _22['cac:Party']) === null || _23 === void 0 ? void 0 : _23['cac:PartyLegalEntity']) === null || _24 === void 0 ? void 0 : _24['cbc:CompanyID']) === null || _25 === void 0 ? void 0 : _25['_text'],
            accessurl: buyerAccessUrl,
            idsid: buyerConnectorId
        },
        seller: {
            name: (_29 = (_28 = (_27 = (_26 = parsedXml[type]['cac:SellerSupplierParty']) === null || _26 === void 0 ? void 0 : _26['cac:Party']) === null || _27 === void 0 ? void 0 : _27['cac:PartyName']) === null || _28 === void 0 ? void 0 : _28['cbc:Name']) === null || _29 === void 0 ? void 0 : _29['_text'],
            scsnid: (_33 = (_32 = (_31 = (_30 = parsedXml[type]['cac:SellerSupplierParty']) === null || _30 === void 0 ? void 0 : _30['cac:Party']) === null || _31 === void 0 ? void 0 : _31['cac:PartyIdentification']) === null || _32 === void 0 ? void 0 : _32['cbc:ID']) === null || _33 === void 0 ? void 0 : _33['_text'],
            tin: (_37 = (_36 = (_35 = (_34 = parsedXml[type]['cac:SellerSupplierParty']) === null || _34 === void 0 ? void 0 : _34['cac:Party']) === null || _35 === void 0 ? void 0 : _35['cac:PartyTaxScheme']) === null || _36 === void 0 ? void 0 : _36['cbc:CompanyID']) === null || _37 === void 0 ? void 0 : _37['_text'],
            kvk: (_41 = (_40 = (_39 = (_38 = parsedXml[type]['cac:SellerSupplierParty']) === null || _38 === void 0 ? void 0 : _38['cac:Party']) === null || _39 === void 0 ? void 0 : _39['cac:PartyLegalEntity']) === null || _40 === void 0 ? void 0 : _40['cbc:CompanyID']) === null || _41 === void 0 ? void 0 : _41['_text'],
            accessurl: sellerAccessUrl,
            idsid: sellerConnectorId
        },
        accounting: {
            name: (_45 = (_44 = (_43 = (_42 = parsedXml[type]['cac:AccountingCustomerParty']) === null || _42 === void 0 ? void 0 : _42['cac:Party']) === null || _43 === void 0 ? void 0 : _43['cac:PartyName']) === null || _44 === void 0 ? void 0 : _44['cbc:Name']) === null || _45 === void 0 ? void 0 : _45['_text'],
            scsnid: (_49 = (_48 = (_47 = (_46 = parsedXml[type]['cac:AccountingCustomerParty']) === null || _46 === void 0 ? void 0 : _46['cac:Party']) === null || _47 === void 0 ? void 0 : _47['cac:PartyIdentification']) === null || _48 === void 0 ? void 0 : _48['cbc:ID']) === null || _49 === void 0 ? void 0 : _49['_text'],
            tin: (_53 = (_52 = (_51 = (_50 = parsedXml[type]['cac:AccountingCustomerParty']) === null || _50 === void 0 ? void 0 : _50['cac:Party']) === null || _51 === void 0 ? void 0 : _51['cac:PartyTaxScheme']) === null || _52 === void 0 ? void 0 : _52['cbc:CompanyID']) === null || _53 === void 0 ? void 0 : _53['_text'],
            kvk: (_57 = (_56 = (_55 = (_54 = parsedXml[type]['cac:AccountingCustomerParty']) === null || _54 === void 0 ? void 0 : _54['cac:Party']) === null || _55 === void 0 ? void 0 : _55['cac:PartyLegalEntity']) === null || _56 === void 0 ? void 0 : _56['cbc:CompanyID']) === null || _57 === void 0 ? void 0 : _57['_text'],
        },
        product: (_61 = (_60 = (_59 = (_58 = parsedXml[type]['cac:OrderLine']) === null || _58 === void 0 ? void 0 : _58['cac:LineItem']) === null || _59 === void 0 ? void 0 : _59['cac:Item']) === null || _60 === void 0 ? void 0 : _60['cbc:Name']) === null || _61 === void 0 ? void 0 : _61['_text'],
        quantity: parseInt((_64 = (_63 = (_62 = parsedXml[type]['cac:OrderLine']) === null || _62 === void 0 ? void 0 : _62['cac:LineItem']) === null || _63 === void 0 ? void 0 : _63['cbc:Quantity']) === null || _64 === void 0 ? void 0 : _64['_text']),
        statusList: []
    };
};
exports.party = (party) => `<cac:Party>
    <cac:PartyIdentification>
        <cbc:ID schemeID="GLN">${party.scsnid}</cbc:ID>
    </cac:PartyIdentification>
    <cac:PartyName>
        <cbc:Name>${party.name}</cbc:Name>
    </cac:PartyName>
    <cac:PartyTaxScheme>
        <cbc:CompanyID>${party.tin}</cbc:CompanyID>
    <cac:TaxScheme/>
    </cac:PartyTaxScheme>
    <cac:PartyLegalEntity>
        <cbc:CompanyID schemeID="NL:KVK">${party.kvk}</cbc:CompanyID>
    </cac:PartyLegalEntity>
    <cac:PhysicalLocation>
        <cbc:Description>${party.idsid}</cbc:Description>
    </cac:PhysicalLocation>
    <cac:Contact>
        <cac:OtherCommunication>
            <cbc:Value>${party.accessurl}</cbc:Value>
        </cac:OtherCommunication>
    </cac:Contact>
</cac:Party>`;
exports.date = (date) => `${date.getFullYear()}-${date.getMonth()}-${date.getDate()}`;
exports.orderMessage = (order) => `<?xml version="1.0" encoding="UTF-8"?>
<${(order.promisedDeliveryDate !== undefined) ? 'OrderResponse' : 'Order'} xmlns="urn:oasis:names:specification:ubl:schema:xsd:Order-2" xmlns:cac="urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2" xmlns:cbc="urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="urn:oasis:names:specification:ubl:schema:xsd:Order-2 http://docs.oasis-open.org/ubl/os-UBL-2.1/xsd/maindoc/UBL-Order-2.1.xsd">
	<!-- SCSN Order example based on UBL Order version 2.1 -->
	<cbc:ID>${order.id}</cbc:ID>
    <cbc:IssueDate>${moment(order.issueDate).format('YYYY-MM-DD')}</cbc:IssueDate>
	<cac:BuyerCustomerParty>
        ${exports.party(order.buyer)}
	</cac:BuyerCustomerParty>
	<cac:SellerSupplierParty>
        ${exports.party(order.seller)}
	</cac:SellerSupplierParty>
	<cac:AccountingCustomerParty>
        ${exports.party(order.accounting)}
	</cac:AccountingCustomerParty>
	<cac:PaymentTerms>
		<cbc:ID>124</cbc:ID>
		<cbc:Note>[Additional information on payment terms]</cbc:Note>
		<cbc:SettlementDiscountPercent>5</cbc:SettlementDiscountPercent>
		<cbc:SettlementDiscountAmount currencyID="EUR">100</cbc:SettlementDiscountAmount>
		<cac:SettlementPeriod>
			<cbc:DurationMeasure unitCode="Days">5.0</cbc:DurationMeasure>
		</cac:SettlementPeriod>
	</cac:PaymentTerms>
	<cac:TransactionConditions>
		<cbc:Description>Accept within 5 days</cbc:Description>
	</cac:TransactionConditions>
	<cac:AnticipatedMonetaryTotal>
		<cbc:PayableAmount currencyID="EUR">1000</cbc:PayableAmount>
	</cac:AnticipatedMonetaryTotal>
	<cac:OrderLine>
		<cac:LineItem>
			<cbc:ID>1</cbc:ID>
			<cbc:Note>[Free-form text note on orderline]</cbc:Note>
			<cbc:LineStatusCode>accepted</cbc:LineStatusCode>
			<cbc:Quantity unitCode="KGM">${order.quantity}</cbc:Quantity>
			<cbc:LineExtensionAmount currencyID="EUR">175</cbc:LineExtensionAmount>
			<cac:Delivery>
				<cbc:ID>${Math.floor(Math.random() * 9000000) + 1000000}</cbc:ID>
				<cbc:Quantity unitCode="KGM">50</cbc:Quantity>
				<cac:DeliveryLocation>
					<cbc:ID schemeID="GLN">${order.buyer.scsnid}</cbc:ID>
				</cac:DeliveryLocation>
				<cac:RequestedDeliveryPeriod>
					<cbc:EndDate>${moment(order.requestedDeliveryDate).format('YYYY-MM-DD')}</cbc:EndDate>
				</cac:RequestedDeliveryPeriod>
                ${(order.promisedDeliveryDate !== undefined) ?
    `<cac:PromisedDeliveryPeriod>
                    <cbc:EndDate>${moment(order.promisedDeliveryDate).format('YYYY-MM-DD')}</cbc:EndDate>
                </cac:PromisedDeliveryPeriod>` : ''}
			</cac:Delivery>
			<cac:Price>
				<cbc:PriceAmount currencyID="EUR">4</cbc:PriceAmount>
				<cbc:BaseQuantity unitCode="KGM">1</cbc:BaseQuantity>
				<cbc:PriceType>net</cbc:PriceType>
				<cac:AllowanceCharge>
					<cbc:ChargeIndicator>false</cbc:ChargeIndicator>
					<cbc:Amount currencyID="EUR">0.5</cbc:Amount>
				</cac:AllowanceCharge>
			</cac:Price>
            <cac:Item>
                <cbc:Description>Sheet of ${order.product}</cbc:Description>
                <cbc:Name>${order.product}</cbc:Name>
                <cac:BuyersItemIdentification>
                    <cbc:ID>${order.product}</cbc:ID>
                </cac:BuyersItemIdentification>
                <cac:SellersItemIdentification>
                    <cbc:ID>${order.product}-1</cbc:ID>
                    <cbc:ExtendedID>A1</cbc:ExtendedID>
                </cac:SellersItemIdentification>
                <cac:Dimension>
                    <cbc:AttributeID>Length</cbc:AttributeID>
                    <cbc:Measure unitCode="MMT">220</cbc:Measure>
                </cac:Dimension>
            </cac:Item>
		</cac:LineItem>
	</cac:OrderLine>
</${(order.promisedDeliveryDate !== undefined) ? 'OrderResponse' : 'Order'}>`;
exports.parseDespatchAdvice = (xml) => { var _a, _b, _c; return (_c = (_b = (_a = convert.xml2js(xml, { compact: true })['DespatchAdvice']) === null || _a === void 0 ? void 0 : _a['cac:OrderReference']) === null || _b === void 0 ? void 0 : _b['cbc:ID']) === null || _c === void 0 ? void 0 : _c['_text']; };
exports.despatchAdvice = (despatch) => `<?xml version="1.0" encoding="UTF-8"?>
<DespatchAdvice xmlns="urn:oasis:names:specification:ubl:schema:xsd:Order-2" xmlns:cac="urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2" xmlns:cbc="urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="urn:oasis:names:specification:ubl:schema:xsd:Order-2 http://docs.oasis-open.org/ubl/os-UBL-2.1/xsd/maindoc/UBL-Order-2.1.xsd">
	<!-- SCSN Order example based on UBL Order version 2.1 -->
	<cbc:ID>${despatch.id}</cbc:ID>
    <cbc:IssueDate>${moment(despatch.issueDate).format('YYYY-MM-DD')}</cbc:IssueDate>
    <cac:OrderReference>
        <cbc:ID>${despatch.orderId}</cbc:ID>
    </cac:OrderReference>
	<cac:BuyerCustomerParty>
        ${exports.party(despatch.buyer)}
	</cac:BuyerCustomerParty>
	<cac:SellerSupplierParty>
        ${exports.party(despatch.seller)}
	</cac:SellerSupplierParty>
	<cac:DeliveryCustomerParty>
        ${exports.party(despatch.buyer)}
	</cac:DeliveryCustomerParty>
	<cac:DespatchSupplierParty>
        ${exports.party(despatch.seller)}
	</cac:DespatchSupplierParty>
	<cac:DespatchLine>
		<cac:LineItem>
			<cac:Item>
                <cbc:Description>Sheet of ${despatch.product}</cbc:Description>
                <cbc:Name>${despatch.product}</cbc:Name>
                <cac:BuyersItemIdentification>
                    <cbc:ID>${despatch.product}</cbc:ID>
                </cac:BuyersItemIdentification>
                <cac:SellersItemIdentification>
                    <cbc:ID>${despatch.product}-1</cbc:ID>
                    <cbc:ExtendedID>A1</cbc:ExtendedID>
                </cac:SellersItemIdentification>
                <cac:ItemInstance>
					<cbc:SerialID>1</cbc:SerialID>
					<cbc:SerialID>2</cbc:SerialID>
					<cbc:SerialID>3</cbc:SerialID>
					<cbc:SerialID>4</cbc:SerialID>
					<cbc:SerialID>5</cbc:SerialID>
					<cbc:SerialID>6</cbc:SerialID>
					<cbc:SerialID>7</cbc:SerialID>
				</cac:ItemInstance>
			</cac:Item>
		</cac:LineItem>
	</cac:DespatchLine>
</DespatchAdvice>`;
//# sourceMappingURL=templates.js.map