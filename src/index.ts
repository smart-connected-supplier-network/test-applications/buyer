'use strict';

import { Application, response } from "express";
import { Server } from "http";
import * as bodyParser from 'body-parser';
import axios from 'axios';
import { Order, orderMessage, parseOrder, parseOrderStatus, Party, parseDespatchAdvice } from "./templates";
const express = require('express');
const util = require('util');

const app: Application = express();

app.use(bodyParser.text({ type: "application/xml" }))
app.use(bodyParser.json());
app.use(express.static('static'));

const forwardAddress: string = process.env.FORWARD || "http://192.168.136.129:8081"

const party: Party = {
    name: process.env.NAME || "SCSN Buyer Part",
    idsid: process.env.IDSID || "https://ids.tno.nl/connector/Buyer-1",
    accessurl: process.env.ACCESSURL || "http://192.168.136.129:8080",
    scsnid: process.env.SCSNID || "urn:scsn:UNKNOWN",
    tin: process.env.TIN || "NL12345",
    kvk: process.env.KVK || "12345"
}

const accounting: Party = {
    name: "Accounting Noord-Brabant",
    scsnid: "urn:scsn:5790001398644",
    tin: "NL234324324S02",
    kvk: "90845456"
}

let server: Server = app.listen(8080, function () {
    console.log('SCSN Buyer started on 0.0.0.0:8080 (http) ');
});

server.setTimeout(120000);

let orders: Map<string, Order> = new Map();

app.use((req, res, next) => setTimeout(next, 100))

// SCSN API

app.post('/api/:version/orderresponse', (req, res) => {
    const parsedOrderResponse = parseOrder(req.body);
    console.log(`Received order response ${parsedOrderResponse.id}`);
    let order = orders.get(parsedOrderResponse.id);
    order.status = "Accepted";
    order.promisedDeliveryDate = parsedOrderResponse.promisedDeliveryDate;
    orders.set(parsedOrderResponse.id, order);
    res.send('');
})

app.post('/api/:version/orderstatus', (req, res) => {
    const parsedOrderStatus = parseOrderStatus(req.body);
    console.log(`Received order status ${parsedOrderStatus.id} ${parsedOrderStatus.statusCode} ${parsedOrderStatus.value}`);
    let order = orders.get(parsedOrderStatus.id);
    order.statusList.push(parsedOrderStatus);
    orders.set(parsedOrderStatus.id, order);
    res.send('');
})

app.post('/api/:version/despatchadvice', (req, res) => {
    const orderId = parseDespatchAdvice(req.body);
    const order = orders.get(orderId);
    console.log(`Received despatch advice ${orderId}`);
    order.status = 'Received despatch advice'
    orders.set(orderId, order);
    res.send('');
});

app.post('/api/:version/invoice', (req, res) => {
    const orderId = req.body;
    const order = orders.get(orderId);
    console.log(`Received invoice ${orderId}`);
    order.status = 'Received invoice'
    orders.set(orderId, order);
    res.send('');
});

app.get('/api/file/:filename', (req, res) => {
    const filename = req.params.filename;
    if (filename == 'aluminium') {
        res.set({"Content-Disposition":`attachment; filename="aluminium"`});
        res.send("Test file aluminium");
    } else if (filename == 'steel') {
        res.set({"Content-Disposition":`attachment; filename="steel"`});
        res.send("Test file steel");
    }
});


// UI Interaction

app.get('/ui/orders', (req, res) => {
    res.send(Array.from(orders.values()));
})

app.post('/ui/orders', (req, res) => {
    const orderId = `${party.scsnid}:Order-${orders.size + 1}`;
    let order = req.body;
    order.id = orderId;
    order.issueDate = new Date(order.issueDate);
    order.requestedDeliveryDate = new Date(order.requestedDeliveryDate);
    order.buyer = party;
    order.accounting = accounting;
    orders.set(orderId, order);
    let orderRequestMessage = orderMessage(order);
    let headers = {};
    if (order.seller.scsnid !== undefined) {
        headers = {
            'Forward-Id': order.seller.scsnid,
            'Forward-Sender': party.scsnid,
            'Content-Type': 'application/xml'
        }
    } else {
        headers = {
            'Forward-Recipient': order.seller.recipient,
            'Forward-To': order.seller.idsid,
            'Forward-Accessurl': order.seller.accessurl,
            'Forward-Sender': party.scsnid,
            'Content-Type': 'application/xml'
        }
    }
    axios.post(`${forwardAddress}/${order.version}/order`, orderRequestMessage, {
        headers: headers,
        timeout: 120000
    }).then(response => {
        order.status = "Placed";
        orders.set(orderId, order);
        res.send({
            status: "OK"
        });
    }).catch(error => {
        console.log(util.inspect(error, {depth: 20}));
        order.status = "Error";
        orders.set(orderId, order);
        res.status(500).send({
            status: "Error",
            message: error
        });
    });
});

app.post('/ui/search', (req, res) => {
    let search = req.body;
    if (search.scsnId !== undefined && search.scsnId !== null && search.scsnId.trim() !== '') {
        axios.get(`${forwardAddress}/1.0.0/broker/party/${search.scsnId}`, {
            headers: {
                'Forward-To': process.env.SCSN_BROKERID,
                'Forward-Sender': party.scsnid,
                'Forward-Recipient': process.env.SCSN_BROKERID,
                'Forward-Accessurl': process.env.SCSN_BROKERURL,
                'Accept': 'application/json'
            },
            timeout: 120000
        }).then(response => {
            res.status(response.status).send(response.data);
        }).catch(error => {
            console.log(util.inspect(error, {depth: 20}));
            res.status(500).send({
                status: "Error",
                message: error
            });
        });
    } else {
        axios.get(`${forwardAddress}/1.0.0/broker/party?name=${encodeURI(search.name || '')}&KVK=${encodeURI(search.kvk || '')}&TIN=${encodeURI(search.tin || '')}`, {
            headers: {
                'Forward-To': process.env.SCSN_BROKERID,
                'Forward-Sender': party.scsnid,
                'Forward-Recipient': process.env.SCSN_BROKERID,
                'Forward-Accessurl': process.env.SCSN_BROKERURL,
                'Accept': 'application/json'
            },
            timeout: 120000
        }).then(response => {
            res.status(response.status).send(response.data);
        }).catch(error => {
            console.log(util.inspect(error, {depth: 20}));
            res.status(500).send({
                status: "Error",
                message: error
            });
        });
    }
});

process.on('SIGTERM', () => {
    server.close(() => {
        console.log('Process terminated');
    })
});
