# SCSN Test Buyer

## Sample deployment (without IDS)

```bash
docker login registry.ids.smart-connected.nl -u market40 -p workshop 
```

```bash
docker-compose up -d
```

Placing an order must be done via cURL (or another program to create HTTP calls):
```bash
curl --location --request POST 'http://localhost:8080/ui/orders' \
--header 'Content-Type: application/json' \
--data-raw '{
	"issueDate": "2020-01-21T15:39:02.373Z",
    "requestedDeliveryDate": "2020-01-25T00:00:00.000Z",
	"seller": {
    	"name": "SCSN Seller Test",
		"idsid": "https://ids.tno.nl/connectors/SCSN-Seller-Test",
		"accessurl": "https://ids.tno.nl/connectors/SCSN-Seller-Test",
		"gln": "23456",
        "tin": "NL23456",
        "kvk": "23456"
    },
    "product": "steel",
	"quantity": 1000
}'
```